//
// Created by zfih on 26/01/2020.
//

#ifndef RAYTRACING_WEEKEND_HITTABLE_HPP
#define RAYTRACING_WEEKEND_HITTABLE_HPP

#include "ray.hpp"

struct hit_record{
    float t;
    vec3 p;
    vec3 normal;
};

class hittable
{
public:
    virtual bool hit(const ray& r, float t_min, float t_max, hit_record& rec) const = 0;
};

#endif //RAYTRACING_WEEKEND_HITTABLE_HPP
