#include <iostream>

#include "vec3.hpp"
#include "ray.hpp"
#include "sphere.hpp"
#include "hittablelist.hpp"
#include "camera.hpp"
#include "random.hpp"


vec3 color(const ray& r, hittable *world){
    hit_record rec;

    if(world->hit(r, 0.0f, MAXFLOAT, rec)){
        return 0.5f * vec3(rec.normal.x() + 1, rec.normal.y() + 1, rec.normal.z() + 1);
    } else {
        vec3 unit_direction = unit_vector(r.direction());
        float t = 0.5f * (unit_direction.y() + 1.0f);
        return (1.0f - t) * vec3(1.0f, 1.0f, 1.0f) + t * vec3(0.5f, 0.7f, 1.0f);
    }
}

int main() {
    int nx = 1000;
    int ny = 500;
    int ns = 2;

    std::cout << "P3\n" << nx << " " << ny << "\n255\n";

    hittable *list[2];
    list[0] = new sphere(vec3(0.0f, 0.0f, -1.0f), 0.5f);
    list[1] = new sphere(vec3(0.0f, -100.5f, -1.0f), 100.0f);
    hittable *world = new hittable_list(list, 2);

    camera cam;

    for (int j = ny; j >= 0; --j) {
        for (int i = 0; i < nx; ++i) {
            vec3 col(0.0f, 0.0f, 0.0f);

            for (int s = 0; s < ns; s++) {
                float u = float(i + c_random_double()) / float(nx);
                float v = float(j + c_random_double()) / float(ny);

                ray r = cam.get_ray(u, v);
                col += color(r, world);
            }

            col /= float(ns);

            int ir = int(255.99 * col.r());
            int ig = int(255.99 * col.g());
            int ib = int(255.99 * col.b());

            std::cout << ir << " " << ig << " " << ib << "\n";
        }
    }

    return 0;
}
