//
// Created by zfih on 26/01/2020.
//

#ifndef RAYTRACING_WEEKEND_RANDOM_HPP
#define RAYTRACING_WEEKEND_RANDOM_HPP

#include <functional>
#include <random>

#include <cstdlib>

inline double c_random_double() {
    return rand() / (RAND_MAX + 1.0);
}

inline double random_double() {
    static std::uniform_real_distribution<double> distribution(0.0, 1.0);
    static std::mt19937 generator;
    static std::function<double()> rand_generator =
            std::bind(distribution, generator);
    return rand_generator();
}

inline float random_float() {
    static std::uniform_real_distribution<float> distribution(0.0f, 1.0f);
    static std::mt19937 generator;
    static std::function<float()> rand_generator =
            std::bind(distribution, generator);
    return rand_generator();
}

#endif //RAYTRACING_WEEKEND_RANDOM_HPP
