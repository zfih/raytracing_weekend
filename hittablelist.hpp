//
// Created by zfih on 26/01/2020.
//

#ifndef RAYTRACING_WEEKEND_HITTABLELIST_HPP
#define RAYTRACING_WEEKEND_HITTABLELIST_HPP

#include "hittable.hpp"

class hittable_list : public hittable
{
public:
    hittable_list() {}
    hittable_list(hittable **l, int n) { list = l; list_size = n; }

    virtual bool hit(const ray& r, float tmin, float tmax, hit_record& rec) const;

    hittable **list;
    int list_size;
};

bool hittable_list::hit(const ray &r, float tmin, float tmax, hit_record &rec) const {
    hit_record temp_rec;
    bool hit_anything = false;
    float closest_so_far = tmax;

    for (int i = 0; i < list_size; ++i) {
        if(list[i]->hit(r, tmin, closest_so_far, temp_rec)){
            hit_anything = true;
            closest_so_far = temp_rec.t;
            rec = temp_rec;
        }
    }

    return hit_anything;
}

#endif //RAYTRACING_WEEKEND_HITTABLELIST_HPP
